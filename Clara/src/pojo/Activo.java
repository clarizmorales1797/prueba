package pojo;

import java.util.Date;

public class Activo {

    public enum Tipos {

        Vehiculos, EquiposComputo, Mobiliario, Edificio
    }
    public int codigo;
    public String nombre;
    public String descripcion;
    public int cantidad;
    public Tipos tipoActivo;
    public float valor;
    public Date fechaCompra;
    public Activo(){}
    public Activo(int codigo, String nombre, String descripcion, int cantidad, Tipos tipoActivo, float valor, Date fechaCompra) {
        this.codigo = codigo;
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.cantidad = cantidad;
        this.tipoActivo = tipoActivo;
        this.valor = valor;
        this.fechaCompra = fechaCompra;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public Tipos getTipoActivo() {
        return tipoActivo;
    }

    public void setTipoActivo(Tipos tipoActivo) {
        this.tipoActivo = tipoActivo;
    }

    public float getValor() {
        return valor;
    }

    public void setValor(float valor) {
        this.valor = valor;
    }

    public Date getFechaCompra() {
        return fechaCompra;
    }

    public void setFechaCompra(Date fechaCompra) {
        this.fechaCompra = fechaCompra;
    }
    
}
