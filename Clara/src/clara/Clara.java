package clara;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import pojo.Activo;
import pojo.Activo.Tipos;

public class Clara {

    static Activo[] list;

    public static void main(String[] args) throws IOException, ParseException {
        BufferedReader read = new BufferedReader(new InputStreamReader(System.in));
        int opc = 0;

        do {
            System.out.println("Menu de opciones");
            System.out.println("1- Agregar nuevo activo fijo");
            System.out.println("2- Visualizar todos los activos fijos");
            System.out.println("3- Calcular depreciación de todos los activos fijos");
            System.out.println("4- Salir");
            System.out.println("Seleccione: ");
            opc = Integer.parseInt(read.readLine());
            switch (opc) {
                case 1: {
                    Activo activo = new Activo();
                    System.out.println("****Datos del activo******");

                    System.out.println("Digitar codigo");
                    activo.setCodigo(Integer.parseInt(read.readLine()));
                    System.out.println("Digitar nombre");
                    activo.setNombre(read.readLine());
                    System.out.println("Digitar descripcion");
                    activo.setDescripcion(read.readLine());
                    System.out.println("Digitar cantidad");
                    activo.setCantidad(Integer.parseInt(read.readLine()));
                    System.out.println("Seleccionar tipo de activo fijo");
                    int opcType = 0;
                    do {
                        System.out.println("1- Vehiculos");
                        System.out.println("2- Equipos de computo");
                        System.out.println("3- Mobiliario");
                        System.out.println("4- Edificio");
                        System.out.println("Seleccionar: ");
                        opcType = Integer.parseInt(read.readLine());
                        switch (opcType) {
                            case 1:
                                activo.tipoActivo = Tipos.Vehiculos;
                                break;
                            case 2:
                                activo.tipoActivo = Tipos.EquiposComputo;
                                break;
                            case 3:
                                activo.tipoActivo = Tipos.Mobiliario;
                                break;
                            case 4:
                                activo.tipoActivo = Tipos.Edificio;
                                break;
                            default:
                                System.out.println("Escoja una opción válida\n");
                                break;
                        }
                    } while (opcType < 1 && opcType > 4);
                    System.out.println("Digitar valor");
                    activo.setValor(Float.parseFloat(read.readLine()));
                    System.out.println("Digitar fecha de compra");
                    String strDate = read.readLine();
                    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yy");
                    Date dt = sdf.parse(strDate);
                    activo.setFechaCompra(dt);
                    System.out.println("Vida util");
                    activo.setVida(Integer.parseInt(read.readLine()));
                    AgregarActivo(activo);
                    break;
                }
                case 2: {
                    if (list == null) {
                        System.out.println("No existen activos fijos\n");
                    } else {
                        System.out.println("Lista de activos fijos");
                        for (int i = 0; i < list.length; i++) {
                            System.out.println("Activo No." + (i + 1));
                            System.out.println("Código: " + list[i].getCodigo());
                            System.out.println("Nombre: " + list[i].getNombre());
                            System.out.println("Descripcion: " + list[i].getDescripcion());
                            System.out.println("Cantidad: " + list[i].getCantidad());
                            if (list[i].tipoActivo == Tipos.Vehiculos) {
                                System.out.println("Tipo de activo: Edificio");
                            } else if (list[i].tipoActivo == Tipos.EquiposComputo) {
                                System.out.println("Tipo de activo: Equipos de computo");
                            } else if (list[i].tipoActivo == Tipos.Mobiliario) {
                                System.out.println("Tipo de activo: Mobiliario");
                            } else {
                                System.out.println("Tipo de activo: Edificio");
                            }
                            System.out.println("Valor: " + list[i].getValor());
                            System.out.println("Fecha: " + list[i].getFechaCompra());
                            System.out.println("Vida util: "+list[i].getVida());
                            System.out.println("");
                        }
                    }
                    break;
                }
                case 3: {
                    System.out.println("Que metodo desea utilizar");
                    System.out.println("1- Método de línea recta");
                    System.out.println("2- Sistema de dígitos de los años incremental");
                    int opcType=Integer.parseInt(read.readLine());
                    do{
                        
                        switch(opcType){
                            
                            case 1:
                            {   
                                float count=0;
                                System.out.println("Metodo linea recta");
                                for(int i=0;i<list.length;i++){
                                    System.out.println("");
                                    System.out.println("Activo: "+list[i].getNombre());
                                    for(int j=0;j<list[i].vida;j++){
                                        System.out.print(" "+(j+1));
                                    }
                                    System.out.println("");
                                    float metodo=list[i].getValor()/list[i].vida;
                                    count+=metodo;
                                    for(int j=0;j<list[i].vida;j++){
                                        System.out.print(""+metodo);
                                    }
                                }
                                System.out.println("Total: "+count);
                                break;
                            }
                            case 2:
                            {
                                for(int i=0;i<list.length;i++){
                                    System.out.println("");
                                    float factor=(list[i].getVida()*(list[i].getVida()+1))/2;
                                    System.out.println("Activo: "+list[i].getNombre());
                                    for(int j=0;j<list[i].vida;j++){
                                        System.out.print(" "+(j+1));
                                    }
                                    System.out.println("");
                                    for(int j=0;j<list[i].getVida();j++){
                                        int valor=list[i].getVida()-j;
                                        float value=valor/factor;
                                        System.out.println(""+(value*list[i].getValor()));
                                    }
                                }
                                break;
                            }
                            default:
                            {
                                System.out.println("Favor escoja una opción válida");
                                break;
                            }
                        }
                    }while(opcType < 1 && opcType > 4);
                    break;
                }
                case 4: {
                    System.out.println("Gracias, vuelva pronto :)");
                    break;
                }
                default: {
                    System.out.println("Escoja una opción valida\n");
                    break;
                }
            }
        } while (opc != 4);
    }

    public static void AgregarActivo(Activo activo) {
        if (list == null) {
            list = new Activo[1];
            list[0] = activo;
        } else {
            int lengthArreglo = list.length;
            Activo[] newArreglo = list;
            list = new Activo[lengthArreglo + 1];
            for (int i = 0; i < lengthArreglo; i++) {
                list[i] = newArreglo[i];
            }
            list[lengthArreglo] = activo;
        }
    }
}
